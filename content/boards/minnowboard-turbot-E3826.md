---
title: Minnowboard Turbot E3826
---

The Minnowboard turbot is a Intel Atom based SBC

![Minnowboard turbot](/img/minnowboard-turbot-E3825.jpg)

specification:
* Intel Atom E3826 SoC
* 2x Intel CPU at 1.46ghz
* 2GB memory
* Intel graphics
* Ethernet port
* 2x USB (1x USB 3, 1x USB 2)
* 1x micro SD card
* 1x SATA II port
* No RTC
* Size: 99x74mm



The board can be bought via
[mouser](https://mouser.com/new/silicom/minnowboard-turbot/). Note that this is
the dual-core not quad core board.

### Power control

The board can be powered on/off via a DC power jack:
* 5V/4A DC input, 2.1mm x 5.5mm, center positive

### Low-level boot control

The board has a 6 pin serial header between the sata port and the microsd 
slot. The pinout is as follows:
* 1: GND (closest to sata)
* 2: CTS
* 3: VCC (3.3v)
* 4: TXD
* 5: RXD
* 6: RTS

This is a pretty common layout for use with FDTI USB to serial cables; These
can be bought on e.g. [mouser](https://mouser.com/ProductDetail/FTDI/TTL-232R-3V3/?qs=sGAEpiMZZMuGxYVy11yKKo9Jh1vSyHd5j3BYkuIZ9TA%3D)

Serial port settings: 115200 8N1

#### Network connectivity

Standard ethernet jack; nothing to see here.

### Bootloader

As an intel system the minnowboard comes with a UEFI bios installed, which
is capable of PXE booting. The recommended way of loading a bootloader is via
this mechanism e.g. grub, which lava can further drive.

Notes on how to build a suitable grub can be found in 
[the apertis setup guide](https://www.apertis.org/reference_hardware/minnowboard_setup/)

### Health checks

* Standard x86\_64 Linux kernel can be used for healthchecks

### Lab notes and trouble shooting

Insert SD cards in the SD card slot for testing.
