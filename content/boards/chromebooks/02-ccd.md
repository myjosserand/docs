---
title: Closed Case Debugging (CCD)
weight: 2
---

Early Chromebook models have a special debug connector on their
motherboard to interface with Servo Micro boards.  This connector is not
fitted on production devices, so a rework is necessary to add it.  Newer
models support the Closed Case Debugging (CCD) feature, using USB
Type-C.  It works with off-the-shelf devices without making any
modifications, the case doesn't need to be opened.  It allows the same
low-level controls as with a Servo Micro.

Interfaces that support CCD include the
[Type-C Servo v4](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/master/docs/servo_v4.md)
and
[SuzyQ](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/master/docs/ccd.md#suzyq-suzyqable).
The control interface that actually has access to the hardware controls is the
[Cr50](https://chromium.googlesource.com/chromiumos/platform/ec/+/cr50_stab/docs/case_closed_debugging_cr50.md)
which is reachable using CCD-capable USB Type-C interfaces.

For security reasons, CCD is not enabled in factory defaults.  A manual
procedure is necessary as described below, ensuring the user
purposefully wants to enable it.


## Connecting a CCD interface

The first step is to connect the host computer (laptop, NUC dispatcher...) to
the Chromebook using a CCD-capable USB Type-C interface.  Each type of
interface has its own particularities as described below.

### Connecting with SuzyQ

The SuzyQ cable contains a controller to enable CCD.  It needs to be
connected on a particular Type-C port of the device, in a particular
orientation.  This will vary depending on the device, so if there are 2x
Type-C ports there will be 4 possibilities but only one that works.

Run the following command in terminal to watch for new devices:

```
sudo dmesg -w
```

Connect the Type-A USB end of the SuzyQ to the host (laptop, NUC
dispatcher...).  A `SuzyQable` device should be detected as shown below:
```
[386753.698883] usb 1-2: new high-speed USB device number 49 using xhci_hcd
[386753.867487] usb 1-2: New USB device found, idVendor=18d1, idProduct=501f
[386753.867505] usb 1-2: New USB device strings: Mfr=10, Product=11, SerialNumber=12
[386753.867510] usb 1-2: Product: SuzyQable
[386753.867514] usb 1-2: Manufacturer: Google Inc.
[386753.867518] usb 1-2: SerialNumber: 0004002101BC
```

Then connect the Type-C end of the SuzyQ cable to the Chromebook.  A `Cr50`
device should be detected by the host in the format of the list below:
```
[387078.927146] usb 1-2.2: new full-speed USB device number 51 using xhci_hcd
[387079.030988] usb 1-2.2: New USB device found, idVendor=18d1, idProduct=5014
[387079.030995] usb 1-2.2: New USB device strings: Mfr=1, Product=2, SerialNumber=11
[387079.031001] usb 1-2.2: Product: Cr50
[387079.031005] usb 1-2.2: Manufacturer: Google Inc.
[387079.031009] usb 1-2.2: SerialNumber: 07809050-93AA4889
```

If it doesn’t, try flipping the Type-C connector and also try other
ports on the Chromebook.

Connect a Type-C USB power adapter to the other port of the Chromebook
as the SuzyQ cable doesn’t provide power.

The serial number of the `Cr50` device is the one to use in the
`/etc/google-servo.conf` file; in the example above `07809050-93AA4889`.
See the section about running [servod](#developer-mode-and-servod)
below.

### Connecting with Type-C Servo v4

_To Do_


## Developer Mode and servod

Before using CCD, please enable Developer Mode as explained in the
[Chromebooks and Servo boards](../01-debugging_interfaces) documentation
and configure `servod` so a service gets started automatically when
connecting the Chromebook.  The produre below requires the `dut-control`
command to work with the Chromebook, at least for the controls that are
active by default.  Enabling CCD will unlock more controls.


## Enabling Closed Case Debugging

See the [CCD documentation from
Google](https://chromium.googlesource.com/chromiumos/platform/ec/+/cr50_stab/docs/case_closed_debugging_cr50.md)
for more details.

### Open a Cr50 console

Find the `Cr50` serial console device by running this command:
```
dut-control -p XXXX cr50_uart_pty
```
where nnnn is the port number used in the file: ‘google-servo.conf’.

For example:
```
# dut-control -p 9932 cr50_uart_pty
cr50_uart_pty:/dev/pts/20
```

Then run minicom with the device path obtained from the previous command.  For
example:
```
minicom -D /dev/pts/20
```

You are now connected to an interactive console with the `Cr50` interface.  Run
the `ccd` command to check the CCD status, it should show “State: Locked” and
“TPM: dev_mode”, as in the example below:
```
2019-12-16 15:32:44 > ccd
2019-12-16 15:32:45 State: Locked
2019-12-16 15:32:45 Password: none
2019-12-16 15:32:45 Flags: 0x000000
2019-12-16 15:32:45 Capabilities: 0000000000000000
2019-12-16 15:32:45   UartGscRxAPTx   Y 0=Default (Always)
2019-12-16 15:32:45   UartGscTxAPRx   Y 0=Default (Always)
2019-12-16 15:32:45   UartGscRxECTx   Y 0=Default (Always)
2019-12-16 15:32:45   UartGscTxECRx   - 0=Default (IfOpened)
2019-12-16 15:32:45   FlashAP         - 0=Default (IfOpened)
2019-12-16 15:32:45   FlashEC         - 0=Default (IfOpened)
2019-12-16 15:32:45   OverrideWP      - 0=Default (IfOpened)
2019-12-16 15:32:45   RebootECAP      - 0=Default (IfOpened)
2019-12-16 15:32:45   GscFullConsole  - 0=Default (IfOpened)
2019-12-16 15:32:45   UnlockNoReboot  Y 0=Default (Always)
2019-12-16 15:32:45   UnlockNoShortPP Y 0=Default (Always)
2019-12-16 15:32:45   OpenNoTPMWipe   - 0=Default (IfOpened)
2019-12-16 15:32:45   OpenNoLongPP    - 0=Default (IfOpened)
2019-12-16 15:32:45   BatteryBypassPP Y 0=Default (Always)
2019-12-16 15:32:45   UpdateNoTPMWipe Y 0=Default (Always)
2019-12-16 15:32:45   I2C             - 0=Default (IfOpened)
2019-12-16 15:32:45   FlashRead       Y 0=Default (Always)
2019-12-16 15:32:45   OpenNoDevMode   - 0=Default (IfOpened)
2019-12-16 15:32:45   OpenFromUSB     - 0=Default (IfOpened)
2019-12-16 15:32:45 read_tpm_nvmem: object at 0x100a not found
2019-12-16 15:32:45 [431.145965 Console unlock allowed]
2019-12-16 15:32:45 TPM: dev_mode
2019-12-16 15:32:45 Capabilities are default.
2019-12-16 15:32:45 Use 'ccd help' to print subcommands
```

If it shows `State: Opened` then CCD has already been enabled on this
device in which case the step below can be skipped.


### Start the CCD open process in the device terminal

In a Chromebook terminal, run the following command:
```
gsctool -a -o
```

Press the power button on the device every time it asks for it, keep
pressing the power button as required.  It will be waiting for a while
before asking again a few times, this can take about 5 minutes to
complete.  It’s apparently a security measure to ensure a real person is
interacting with the device.

The Chromebook will reboot automatically at the end of the procedure.

Then run `ccd` again in the Cr50 shell, it should show “State: Opened” this
time:
```
2019-12-16 15:43:59 State: Opened
2019-12-16 15:43:59 Password: none
2019-12-16 15:43:59 Flags: 0x000000
2019-12-16 15:43:59 Capabilities: 0000000000000000
2019-12-16 15:43:59   UartGscRxAPTx   Y 0=Default (Always)
2019-12-16 15:43:59   UartGscTxAPRx   Y 0=Default (Always)
2019-12-16 15:43:59   UartGscRxECTx   Y 0=Default (Always)
2019-12-16 15:43:59   UartGscTxECRx   Y 0=Default (IfOpened)
2019-12-16 15:43:59   FlashAP         Y 0=Default (IfOpened)
2019-12-16 15:43:59   FlashEC         Y 0=Default (IfOpened)
2019-12-16 15:43:59   OverrideWP      Y 0=Default (IfOpened)
2019-12-16 15:43:59   RebootECAP      Y 0=Default (IfOpened)
2019-12-16 15:43:59   GscFullConsole  Y 0=Default (IfOpened)
2019-12-16 15:43:59   UnlockNoReboot  Y 0=Default (Always)
2019-12-16 15:43:59   UnlockNoShortPP Y 0=Default (Always)
2019-12-16 15:43:59   OpenNoTPMWipe   Y 0=Default (IfOpened)
2019-12-16 15:43:59   OpenNoLongPP    Y 0=Default (IfOpened)
2019-12-16 15:43:59   BatteryBypassPP Y 0=Default (Always)
2019-12-16 15:43:59   UpdateNoTPMWipe Y 0=Default (Always)
2019-12-16 15:43:59   I2C             Y 0=Default (IfOpened)
2019-12-16 15:43:59   FlashRead       Y 0=Default (Always)
2019-12-16 15:43:59   OpenNoDevMode   Y 0=Default (IfOpened)
2019-12-16 15:43:59   OpenFromUSB     Y 0=Default (IfOpened)
2019-12-16 15:43:59 read_tpm_nvmem: object at 0x100a not found
2019-12-16 15:43:59 [1107.234474 Console unlock allowed]
2019-12-16 15:43:59 TPM:
2019-12-16 15:43:59 Capabilities are default.
2019-12-16 15:43:59 Use 'ccd help' to print subcommands
```

Enable these options in the Cr50 shell:
```
> ccd set OpenFromUSB Always
> ccd set OpenNoDevMode Always
```

Enable the “testlab” mode:
```
> ccd testlab enable
```

Press the power button several times as required.

Now CCD can be opened with this command:
```
> ccd testlab open
```

If this worked as expected, congratulations!  The CCD setup is now
complete for the device.

However, if at any point the Chromebook device becomes unresponsive and
needs the operating system to be recovered, follow the
[Recovery](../01-debugging_interfaces#recovery) procedure and start again.
