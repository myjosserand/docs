---
title: Common issues
---

This is a list of the issues we found that were common for all the
Chromebooks tested in the lab.


## Issues related to Servo v4

The Servo v4 debugging interface causes different issues depending on
the Chromebook board and the lab setup.

### Network interface issues

#### R8152 driver not reliable at Gigabit speeds

The Realtek R8152 USB-Eth adapter can be found in the Servo v4 and in
other standalone dongles.

The R8152 Ethernet driver in Depthcharge is not reliable when working at
Gigabit speeds. It's recommeded to configure the link as Fast Ethernet
(100 Mbps) when booting over TFTP.


#### Ethernet frame reception issues

Ethernet frame reception may fail and hang Depthcharge when using the
Servo v4 as an USB-Eth adapter (error: `R8152: Bulk read error`). Bigger
file transfers make this more likely to happen, although it's not
completely deterministic.


### Stability issues

#### Debugging interface unstable when using Servo v4 connected to power supply

Observed in:

- `coral`: R64-10068.111.0.serial
- `rammus`: R72-11275.110.0.serial
- `octopus`: R72-1197.144.0.serial

with Servo v4 FW 2.3.22 and 2.4.35.

When the Servo v4 is connected to a power supply the debugging
interface becomes unstable. Some of the effects seen:

  - Charging is unreliable.
  - Serial consoles stop working. The following message appears in the
    kernel log:

```
google ttyUSB4: usb_serial_generic_write_bulk_callback - urb stopped: -32
```
  - Ethernet data transfer through the Servo fails.
  - Cr50 commands lost sometimes.

Basically, all communication going through the Servo starts failing
intermittently.
