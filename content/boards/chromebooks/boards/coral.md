---
title: Coral Chromebooks
---

`coral` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

  - [Lenovo 100e
  Chromebook](https://www.lenovo.com/us/en/laptops/lenovo/lenovo-n-series/100e-Chromebook/p/88ELC1S9999)
  - [Acer Chromebook Spin
  11](https://www.acer.com/ac/en/AU/press/2018/333192)
  - [ASUS Chromebook C523](https://www.asus.com/us/Laptops/ASUS-Chromebook-C523NA/)

The specs for these chromebooks vary in terms of display, connectivity
and devices, but they are based on Intel Apollo Lake 64 bit CPUs
(Celeron Dual-Core N3350 , N3450 or Quad-Core N4200).

The Collabora LAVA lab contains the following `coral` devices:

  - ASUS Chromebook C523NA
    - [`asus-C523NA-A20057-coral-cbg-0`](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-0)
    - [`asus-C523NA-A20057-coral-cbg-1`](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-1)
    - [`asus-C523NA-A20057-coral-cbg-2`](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-2)
    - [`asus-C523NA-A20057-coral-cbg-3`](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-3)



### Debugging interfaces

`coral` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In an Asus C523N, the debug port is the USB-C port in the left side
(also used for the power supply).

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

  - [`coral-1`](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-1)
    fails to boot in around 5% of the attempts. One of the Coreboot
    stages resets the board and then it never comes out of reset (last
    console message: `CSE timed out. Resetting`).

See also [Common issues](../common_issues.md).

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xde000000,115200n8 console=ttyS2,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

The IP and path of the NFS share are examples and should be adapted to
the test setup.
